import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.isj.messenger',
  appName: 'messenger',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
